from typing import Any, Generic, Self, Tuple, Type, TypeVar

from databases import Database
from databases.interfaces import Record
from pydantic import BaseModel
from sqlalchemy import Column, ColumnExpressionArgument, Select, exists, select
from sqlalchemy.sql.functions import count

from fastapi_orm.table import Table

ModelType = TypeVar('ModelType', bound=BaseModel)
TableType = TypeVar('TableType', bound=Table)
ColumnAny = Column[Any]


class QueryManager(Generic[TableType, ModelType]):
    def __init__(self, db: Database, table: Type[TableType], model: Type[ModelType]) -> None:
        self._table = table
        self._model = model
        self.__stmt: Select[Any] = Select(self._table)
        self._db: Database = db
        self.__related_fields: dict[str, Tuple[str, ColumnAny]] = {}

    async def get(self) -> ModelType | None:
        row = await self._db.fetch_one(self.__stmt)
        return self.__map_row(row) if row else None

    async def all(self) -> list[ModelType]:
        rows = await self._db.fetch_all(self.__stmt)
        return [self.__map_row(row) for row in rows]

    async def exists(self) -> bool:
        return bool(await self._db.fetch_val(select(exists(self.__stmt))))

    async def count(self) -> int:
        return int(await self._db.fetch_val(select(count()).select_from(self.__stmt.subquery())))

    def get_statement(self) -> Select[Any]:
        return self.__stmt

    def _add_columns(self, *columns: Any) -> Self:
        self.__stmt = self.__stmt.add_columns(*columns)
        return self

    def _where(self, *conditions: ColumnExpressionArgument[bool]) -> Self:
        self.__stmt = self.__stmt.where(*conditions)
        return self

    def _order_by(self, *order_by: Any) -> Self:
        self.__stmt = self.__stmt.order_by(*order_by)
        return self

    def _join(self, table: Any, on: ColumnExpressionArgument[bool], isouter: bool = False) -> Self:
        self.__stmt = self.__stmt.join(table, on, isouter=isouter)
        return self

    def _select_related(self, table: Any, alias: str) -> Self:
        mapping = {
            '{0}__{1}'.format(alias, col.name): (alias, getattr(table, col.name))
            for col in table.__table__.c
        }
        self.__related_fields.update(mapping)
        self._add_columns(*[col.label(label) for label, (_, col) in mapping.items()])
        return self

    def __map_row(self, record: Record) -> ModelType:
        row = dict(record)
        if not self.__related_fields:
            return self._model(**row)
        for label, (alias, col) in self.__related_fields.items():
            if row[label] is None:
                continue
            alias_parts = alias.split('.')
            update_row = row
            for alias_part in alias_parts:
                if update_row.get(alias_part) is None:
                    update_row[alias_part] = {}
                update_row = update_row[alias_part]
            update_row[col.key] = row[label]
        return self._model(**row)
