from typing import Any, Generic, Type, TypeVar

from databases import Database
from databases.interfaces import Record
from sqlalchemy import ColumnExpressionArgument, delete, insert, update

from fastapi_orm.exception import DatabaseError
from fastapi_orm.table import Table

TableType = TypeVar('TableType', bound=Table)


class DataManager(Generic[TableType]):
    def __init__(self, db: Database, table: Type[TableType]) -> None:
        self._db = db
        self._table = table

    async def _insert(self, data: dict[str, Any]) -> Record:
        stmt = insert(self._table).values(**data).returning(*self._table.__table__.primary_key)
        row = await self._db.fetch_one(stmt)
        if not row:
            raise DatabaseError('Insert error, no data return')
        return row

    async def _insert_many(self, data: list[dict[str, Any]]) -> list[Record]:
        stmt = insert(self._table).values(data).returning(*self._table.__table__.primary_key)
        rows = await self._db.fetch_all(stmt)
        if len(rows) != len(data):
            raise DatabaseError('Insert error, not all data saved')
        return rows

    async def _update(self, condition: ColumnExpressionArgument[bool], data: dict[str, Any]) -> list[Record]:
        stmt = update(self._table).where(condition).values(**data).returning(*self._table.__table__.primary_key)
        return await self._db.fetch_all(stmt)

    async def _delete(self, condition: ColumnExpressionArgument[bool]) -> list[Record]:
        stmt = delete(self._table).where(condition).returning(*self._table.__table__.primary_key)
        return await self._db.fetch_all(stmt)
