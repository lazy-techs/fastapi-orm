from datetime import datetime

from sqlalchemy import DateTime, Text
from sqlalchemy.orm import DeclarativeBase


class Table(DeclarativeBase):
    abstract = True
    type_annotation_map = {
        datetime: DateTime(timezone=True),
        str: Text,
    }
