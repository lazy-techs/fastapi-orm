import os

import pytest
from databases import Database
from sqlalchemy import create_engine, text


@pytest.fixture(scope='session')
def event_loop():
    import asyncio

    return asyncio.get_event_loop()


@pytest.fixture(scope='session')
def database_engine():
    from fastapi_orm.table import Table

    db_url = os.environ.get('TEST_DB_URL', 'postgresql://postgres:postgres@localhost:15433/postgres')
    engine = create_engine(db_url, isolation_level='AUTOCOMMIT')
    Table.metadata.create_all(engine)

    yield engine
    engine.dispose()


@pytest.fixture(scope='session')
def database_connection(database_engine):
    with database_engine.connect() as connection:
        yield connection


@pytest.fixture(scope='session')
async def async_db(database_engine, event_loop):
    db_url = os.environ.get('TEST_DB_URL', 'postgresql://postgres:postgres@localhost:15433/postgres')
    db = Database(db_url)
    await db.connect()
    yield db
    await db.disconnect()


@pytest.fixture(autouse=True)
def clean_tables(database_engine):
    from fastapi_orm.table import Table

    with database_engine.connect() as connection:
        for table in Table.metadata.sorted_tables:
            connection.execute(text(f'TRUNCATE TABLE "{table.name}" RESTART IDENTITY CASCADE'))
