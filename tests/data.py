from datetime import datetime
from typing import Self

from databases import Database
from databases.interfaces import Record
from pydantic import BaseModel
from sqlalchemy import BigInteger, Boolean, DateTime, ForeignKey, Text, text
from sqlalchemy.orm import Mapped, mapped_column

from fastapi_orm.data_manager import DataManager
from fastapi_orm.query_manager import QueryManager
from fastapi_orm.table import Table


class PhotoTable(Table):
    __tablename__ = 'photo'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    path: Mapped[str] = mapped_column(Text)


class UserTable(Table):
    __tablename__ = 'user'

    id: Mapped[int] = mapped_column(BigInteger, primary_key=True, autoincrement=True)
    active: Mapped[bool] = mapped_column(Boolean, server_default=text('true'))
    photo_id: Mapped[int] = mapped_column(ForeignKey('photo.id'), nullable=True)
    email: Mapped[str] = mapped_column(Text)
    created_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), server_default=text('now()'))
    updated_at: Mapped[datetime] = mapped_column(DateTime(timezone=True), server_default=text('now()'))


class Photo(BaseModel):
    id: int
    path: str


class User(BaseModel):
    id: int
    email: str
    photo_id: int | None = None
    photo: Photo | None = None
    created_at: datetime | None = None
    updated_at: datetime | None = None


class UserIn(BaseModel):
    email: str
    photo_id: int | None = None


class UserQueryManager(QueryManager[UserTable, User]):
    def __init__(self, db: Database) -> None:
        super().__init__(
            db=db,
            table=UserTable,
            model=User,
        )
        self._order_by(self._table.id.asc())

    def by_pk(self, pk: int) -> Self:
        return self._where(self._table.id == pk)

    def by_email(self, email: str) -> Self:
        return self._where(self._table.email == email)

    def with_photo(self) -> Self:
        return (
            self._join(PhotoTable, PhotoTable.id == self._table.photo_id, isouter=True)
                ._select_related(PhotoTable, 'photo')
        )


class UserDataManager(DataManager[UserTable]):
    def __init__(self, db: Database) -> None:
        super().__init__(
            db=db,
            table=UserTable,
        )

    async def create(self, data: UserIn) -> Record:
        return await self._insert(data.model_dump(mode='json'))

    async def update_by_id(self, user_id: int, data: UserIn) -> Record:
        records = await self._update(
            self._table.id == user_id,
            data={
                **data.model_dump(exclude_unset=True, mode='json'),
                'updated_at': text('now()'),
            },
        )
        (record,) = records
        return record

    async def delete_by_id(self, user_id: int) -> Record:
        records = await self._delete(self._table.id == user_id)
        (record,) = records
        return record
