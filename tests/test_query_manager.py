from datetime import datetime, timezone

from fastapi_test_utils import loaddata

from tests.data import Photo, PhotoTable, User, UserQueryManager, UserTable


async def test_query_manager_get_by_pk(database_connection, async_db):
    loaddata(database_connection, UserTable, [{'id': 1, 'email': 'test@test.test'}])
    user = await UserQueryManager(db=async_db).by_pk(1).get()
    assert user.id == 1
    assert user.email == 'test@test.test'


async def test_query_manager_get_filtered(database_connection, async_db):
    loaddata(
        database_connection,
        UserTable,
        [
            {'id': 1, 'email': 'test@test.test'},
            {'id': 2, 'email': 'asdf@asdf.asdf'},
        ],
    )
    user = await UserQueryManager(db=async_db).by_email('asdf@asdf.asdf').get()
    assert user.id == 2


async def test_query_manager_select_related(database_connection, async_db):
    loaddata(
        database_connection,
        PhotoTable,
        [{'id': 1, 'path': 'path1'}, {'id': 2, 'path': 'path2'}],
    )
    loaddata(
        database_connection,
        UserTable,
        [
            {
                'id': 1,
                'email': 'test@test.test',
                'photo_id': 1,
                'created_at': '2024-11-01T11:00:00',
                'updated_at': '2024-11-01T11:01:01',
            },
            {
                'id': 2,
                'email': 'asdf@asdf.asdf',
                'photo_id': 2,
                'created_at': '2024-11-01T12:00:00',
                'updated_at': '2024-11-01T12:02:01',
            },
            {
                'id': 3,
                'email': 'kfjgkf@kfjgkf.fkf',
                'created_at': '2024-11-01T13:00:00',
                'updated_at': '2024-11-01T13:02:01',
            },
        ],
    )
    users = await UserQueryManager(db=async_db).with_photo().all()
    assert users == [
        User(
            id=1,
            email='test@test.test',
            photo_id=1,
            photo=Photo(id=1, path='path1'),
            created_at=datetime(2024, 11, 1, 11, 0, 0, tzinfo=timezone.utc),
            updated_at=datetime(2024, 11, 1, 11, 1, 1, tzinfo=timezone.utc),
        ),
        User(
            id=2,
            email='asdf@asdf.asdf',
            photo_id=2,
            photo=Photo(id=2, path='path2'),
            created_at=datetime(2024, 11, 1, 12, 0, 0, tzinfo=timezone.utc),
            updated_at=datetime(2024, 11, 1, 12, 2, 1, tzinfo=timezone.utc),
        ),
        User(
            id=3,
            email='kfjgkf@kfjgkf.fkf',
            created_at=datetime(2024, 11, 1, 13, 0, 0, tzinfo=timezone.utc),
            updated_at=datetime(2024, 11, 1, 13, 2, 1, tzinfo=timezone.utc),
        ),
    ]


async def test_query_manager_count(database_connection, async_db):
    loaddata(
        database_connection,
        PhotoTable,
        [{'id': 1, 'path': 'path1'}, {'id': 2, 'path': 'path2'}],
    )
    loaddata(
        database_connection,
        UserTable,
        [
            {
                'id': 1,
                'email': 'test@test.test',
                'photo_id': 1,
                'created_at': '2024-11-01T11:00:00',
                'updated_at': '2024-11-01T11:01:01',
            },
            {
                'id': 2,
                'email': 'asdf@asdf.asdf',
                'photo_id': 2,
                'created_at': '2024-11-01T12:00:00',
                'updated_at': '2024-11-01T12:02:01',
            },
            {
                'id': 3,
                'email': 'kfjgkf@kfjgkf.fkf',
                'created_at': '2024-11-01T13:00:00',
                'updated_at': '2024-11-01T13:02:01',
            },
        ],
    )
    cnt = await UserQueryManager(db=async_db).with_photo().count()
    assert cnt == 3
