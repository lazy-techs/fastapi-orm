from fastapi_test_utils import loaddata
from sqlalchemy import text

from tests.data import UserDataManager, UserIn, UserTable


async def test_data_manager_create(async_db, database_connection):
    row = await UserDataManager(db=async_db).create(data=UserIn(email='test@test.test'))
    assert row
    user = database_connection.execute(text('SELECT * FROM "user" where id=1')).fetchone()._asdict()
    assert user['id'] == row['id']
    assert user['email'] == 'test@test.test'


async def test_data_manager_update_by_id(async_db, database_connection):
    loaddata(database_connection, UserTable, [{'id': 1, 'email': 'test@test.test'}])
    row = await UserDataManager(db=async_db).update_by_id(user_id=1, data=UserIn(email='asdf@asdf.asdf'))
    assert row
    user = database_connection.execute(text('SELECT * FROM "user" where id=1')).fetchone()._asdict()
    assert user['email'] == 'asdf@asdf.asdf'
    assert user['created_at'] != user['updated_at']


async def test_data_manager_delete_by_id(async_db, database_connection):
    loaddata(database_connection, UserTable, [{'id': 1, 'email': 'test@test.test'}])
    row = await UserDataManager(db=async_db).delete_by_id(user_id=1)
    assert row['id'] == 1
    user = database_connection.execute(text('SELECT * FROM "user" where id=1')).fetchone()
    assert not user
